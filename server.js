var http = require('http');
var express = require('express');
var wpi = require('wiring-pi');

var PLAYER_ONE_BUTTON_PIN = 0;
var PLAYER_TWO_BUTTON_PIN = 2;

var app = express();
app.server = http.createServer(app);





/*var io = require('socket.io').listen(1337);

io.sockets.on('connection', function (socket) {
  console.log("New connection");

  socket.on('change-background', function (msg) {
    io.sockets.emit('update-background', { color: randomColor() });
  });

  socket.on('disconnect', function () {
    io.sockets.emit('user disconnected');
  });
});
*/
function randomColor() {
	return '#'+(Math.random()*0xFFFFFF<<0).toString(16);
}






var WebSocket = require('ws');
var wss = new WebSocket.Server({
    server: app.server,
    port: 1337
});

wss.broadcast = function(data) {
    for (var i in this.clients)
        this.clients[i].send(data);
};

// use like this:
wss.on('connection', function(ws) {
    ws.on('message', function(message) {
        wss.broadcast(randomColor());
    });
});

wpi.setup('wpi');
wpi.pinMode(PLAYER_ONE_BUTTON_PIN, wpi.INPUT);
wpi.pullUpDnControl(PLAYER_ONE_BUTTON_PIN, wpi.PUD_UP);
wpi.wiringPiISR(PLAYER_ONE_BUTTON_PIN, wpi.INT_EDGE_FALLING, function() {
    wss.broadcast(randomColor());
    wpi.delay(1000);
});

//Pin settings for player two button
wpi.pinMode(PLAYER_TWO_BUTTON_PIN, wpi.INPUT);
wpi.pullUpDnControl(PLAYER_TWO_BUTTON_PIN, wpi.PUD_UP);
wpi.wiringPiISR(PLAYER_TWO_BUTTON_PIN, wpi.INT_EDGE_FALLING, function() {
    wss.broadcast(randomColor());
    wpi.delay(1000);
});

app.use(express.static('./'));

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});